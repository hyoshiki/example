import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    MainForm {
        anchors.fill: parent
        mouseArea.onClicked: {
            Qt.quit();
        }

        Rectangle {
            id: rectangle1
            x: 128
            y: 34
            width: 170
            height: 70
            color: "#e11414"

            Text {
                id: text1
                x: 1
                y: 21
                text: qsTr("In the red box")
                font.pixelSize: 24
            }
        }
    }
}
